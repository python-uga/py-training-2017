#!/usr/bin/env python3

from typing import Iterable, Mapping, Union


def read_file(filename: str) -> str:
    with open(filename) as handle:
        text = handle.read().replace("\n", "")
    return text


def extract_pattern(text: str, pattern_size: int) -> list[str]:
    """extract all the pattern of size pattern_size and return a list of them
    """
    patterns = []
    for i in range(pattern_size, len(text)+1):
        patterns.append(text[i-pattern_size:i])
    return patterns


def count_elem(sequence: Iterable[str]) -> dict[str, int]:
    d = {}
    for elem in sequence:
        try:
            d[elem.lower()] += 1
        except KeyError:
            d[elem.lower()] = 1
    return d


def predict(pattern: str, count_dic: Mapping[str, int]) -> tuple[Union[None, str], int]:
    maximum = -1
    best = None
    for key, value in count_dic.items():
        if key.startswith(pattern):
            if value > maximum:
                best = key
                maximum = value
    return (best, maximum)


if __name__ == "__main__":
    text = read_file("tom_sawyer.txt")
    l = extract_pattern(text, 3)
    counts = count_elem(l)
# print(f"counts = {counts}")

    while True:
        to_predict = input("give your prefix (quit for quit)")
        to_predict = to_predict.lower()
        if to_predict.startswith("quit"):
            break
        print(f"predicting {to_predict}", end=" ")
        predicted = predict(to_predict, counts)
        print(f"best prediction: {predicted[0]} (with count {predicted[1]})")
