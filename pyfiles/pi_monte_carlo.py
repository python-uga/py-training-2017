#!/bin/env python3

import numpy as np
import math


def in_circle(n: int, verbose: bool = False) -> float:
    """ tir n points dans le carré de côté 1 et retourne le nombre de fois
    où le point est tombé dans le cercle inscrit
    Note: n est supposé être positif, pas de vérification
    :input n: (int) le nombre de tirages aléatoire réalisés
    :output: (float) le nombre de fois où le point est tiré dans le cercle
    """
    arrX = np.random.random(n)-0.5
    arrY = np.random.random(n)-0.5
    arrDist = np.sqrt(arrX*arrX + arrY*arrY)
    if verbose:
        print(f"arrX = {arrX} arrY={arrY}")
    return len(arrDist[arrDist < 0.5])


def main(nb_trials: int):
    """run nb_trias to estimate pi """
    nb_tot_in_circle = 0
    nb_tot = 0
    for n in range(100):
        nb_tot += nb_trials
        nb_tot_in_circle += in_circle(nb_trials)
        pi_tilde = 4 * nb_tot_in_circle/nb_tot
    return pi_tilde


if __name__ == "__main__":
    pi_tilde = main(1000000)
    print(f"pi ~ {pi_tilde:.15f} diff : {abs(math.pi - pi_tilde)}")
