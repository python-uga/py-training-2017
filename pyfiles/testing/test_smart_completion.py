#!/usr/bin/env python3

import smart_completion as sc


def test_extract_pattern():
    """ test the extract pattern function"""
    s = "abcdefghijklmnopq"
    patterns = sc.extract_pattern(s, 3)
    assert patterns[0] == "abc"
    assert patterns[-1] == "opq"
    assert len(patterns) == len(s) - 2


def test_count_elem():
    """ count the elements in a iterable (over str) """
    seq = ["abc", "def", "abc"]
    dic = sc.count_elem(seq)
    assert dic["def"] == 1
    assert dic["abc"] == 2
    assert len(dic) == 2


def test_predict():
    """ test predic """
    dic = {"abc": 2, "abd": 1, "def": 123, "aac": 12}
    best, maximum = sc.predict("ab", dic)
    assert best == "abc"
    assert maximum == 2

    best, maximum = sc.predict("a", dic)
    assert best == "aac"
    assert maximum == 12
