#!/usr/bin/env python3

import weather_station as ws


def test_weather_station_all_perceived_temps():
    """Test weather_station perceived_temp"""
    paris = ws.WeatherStation([10, 0, -30], [10.0, 5.0, 1.0])
    perceived_temps = paris.all_perceived_temp()
    assert perceived_temps == [9.0, 5.0, -2.0]


def test_weather_station_max_temp():
    """Test weather_station max temp func """
    paris = ws.WeatherStation([40, 0, 20, 30, 20, 0], [8.0, 5.0, 1.0, -1.0, -1.0, 3.0])
    assert paris.max_temp(perceived=False) == 8.0
    assert paris.max_temp(perceived=True) == 5.0


def test_weather_station_arg_max_temp():
    """Test weather_station arg max temp func """
    paris = ws.WeatherStation([40, 0, 20, 30, 20, 0], [8.0, 5.0, 1.0, -1.0, -1.0, 3.0])
    assert paris.arg_max_temp(perceived=True) == 1
    assert paris.arg_max_temp(perceived=False) == 0


def test_weather_station_perceived_temp():
    """Test weather_station perceived_temp"""
    paris = ws.WeatherStation([10, 0, 20, 30, 20, 0], [10.0, 5.0, 1.0, -1.0, -1.0, 3.0])
    assert paris.perceived_temp(10, 0) == 10
    assert paris.perceived_temp(10, 20) == 8.0
    # print(f"max temp is {paris.max_temp()}°C at index {paris.arg_max_temp()}")
    # print(f"wind speed at max temp = {paris.wind[idx_max_temp]} km/h")
