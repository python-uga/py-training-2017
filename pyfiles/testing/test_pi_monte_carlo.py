#!/bin/env python3

import numpy as np
import pi_monte_carlo
import math


def test_in_circle() -> None:
    """ test in_circle with a small set """
    np.random.seed(12345)
    nb_in_circle = pi_monte_carlo.in_circle(4, True)
    assert nb_in_circle == 3, f"nb_in_circle={nb_in_circle}, expecting 3"

    nb_in_circle = pi_monte_carlo.in_circle(4, False)
    assert nb_in_circle == 3, f"nb_in_circle={nb_in_circle}, expecting 3"


def test_pi_monte_carlo_main():
    """ test estimation of pi_tilde by monte carlo method """
    np.random.seed(12345)
    pi_tilde = pi_monte_carlo.main(1000000)
    assert pi_tilde - math.pi < 1e-10
