#!/usr/bin/env python3

import prime


def test_prime():
    """ test the prime function """
    assert prime.is_prime(1) is False
    assert prime.is_prime(2) is True
    assert prime.is_prime(7) is True
    assert prime.is_prime(97) is True
    assert prime.is_prime(-97) is False
