#!/usr/bin/env python3

class WeatherStation(object):
    """ A weather station that holds wind and temperature
    :param wind: any ordered iterable
    :param temperature: any ordered iterable
    wind and temperature must have the same length.
    """

    def __init__(self, wind: list[int], temperature: list[float]):
        self.wind = [abs(x) for x in wind]
        self.temp = list(temperature)
        if len(self.wind) != len(self.temp):
            raise ValueError(
                "wind and temperature should have the same size"
            )

    def max_temp(self, perceived=True) -> float:
        """ returns the maximum temperature recorded in the station"""
        if perceived:
            return max(self.all_perceived_temp())
        else:
            return max(self.temp)

    def arg_max_temp(self, perceived=True) -> int:
        """ returns the index of (one of the) maximum temperature recorded in the station"""
        if perceived:
            temp = self.all_perceived_temp()
            return temp.index(self.max_temp(temp))
        else:
            return self.temp.index(self.max_temp(perceived=False))

    def perceived_temp(self, temp: float, wind: int) -> float:
        """returns the perceived temp given a temp and a wind speed """
        if wind == 0:
            return temp
        else:
            return temp - wind/10

    def perceived_temp_at(self, index) -> float:
        return self.perceived_temp(self.temp[index], self.wind[index])

    def all_perceived_temp(self) -> list[float]:
        return [self.perceived_temp_at(index) for index in range(len(self.wind))]
