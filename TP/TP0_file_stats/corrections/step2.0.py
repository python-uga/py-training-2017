#!/usr/bin/env python3
"""Checking file content (supposely 3 fields, p1, p2, p3, each field of the
form key=val where key is p1, p2 or p3, and val is a float.

"""

import re
import logging
logging.basicConfig(level=logging.DEBUG)

def compute_stats(file_name):
    """
    computes the statistics of data in file_name
    :param file_name: the name of the file to process
    :type file_name: str
    :return: the statistics
    :rtype: a tuple (number, sum, average)
    """
    total_sum = 0.0
    number = 0
    with open(file_name) as handle:
        for line in handle:
            if line.startswith('#'):
                continue
            elem = float(line)
            total_sum += elem
            number += 1

    return (number, total_sum, total_sum/float(number))


def check_format_re(file_name, default_values):
    """Check the file has the right format

    :param file_name: (str) the input file name (text)
    :param default_values: (array of float) the default values for
                           p1, p2, and p3 respectivelly
    :returns: the read values
    """
    with open(file_name) as handle:
        for line_num, line in enumerate(handle):
            vals = [None, None, None]
            logging.debug("line = {}".format(line.strip()))
            for si, sv in re.findall(r"p(\d)\s*=\s*(\d\.\d+)", line):
                try:
                    idx = int(si) - 1
                except ValueError as e:
                    logging.warning(f'file {file_name} line {line_num}: wrong format: p{si}')
                    continue
                if idx > 2:
                    logging.warning(f'file {file_name} line {line_num}: p{si} out of bounds')
                    continue
                try:
                    val = float(sv)
                except ValueError as e:
                    logging.warning(f'file {file_name} line {line_num}: wrong value: p{si}')
                    continue
                vals[idx] = val
            # checking for missing item
            for i,v in enumerate(vals):
                if v is None:
                    logging.warning(f'file {file_name} line {line_num}: missing: p{i+1}, setting default value')
                    vals[i] = default_values[i]


def check_format(file_name, cols_names_set):
    """
        check the file has the right format.

       :param file_name: the input file name
       :type file_name: str
       :param cols_names_set:  the columns names we want to have
       :type cols_names_set: set of str
    """
    print('checking ' + file_name)
    nb_fields = len(cols_names_set)
    with open(file_name) as handle:
        for line_num, line in enumerate(handle):
            cols = line.split()
            if len(cols) != nb_fields:
                print('line {} contains only {} fields, expecting {}'.format(
                    line_num, len(cols), nb_fields))
                continue
            # extracting field name
            field_names = set()
            for col in cols:
                if '=' not in col:
                    print('line {}: malformed column ({})'.format(
                        file_name, line_num, col))
                    continue
                field_name = col.split('=')[0]
                field_names.add(field_name)
            if cols_names_set != field_names:
                print(('line {}: keys do not match the required keys: '
                       'problem with keys {}').format(
                    line_num, cols_names_set ^ field_names))


cols_names_set = set(['p1', 'p2', 'p3'])
file_names = ['../data/file_mut_cols.txt',
              '../data/file_mut_cols_with_error.txt']

if __name__ == "__main__":
    numbers = []
    sums = []

    for file_name in file_names:
        check_format(file_name, cols_names_set)

# using version with re, and type check
    print("USING VERSION WITH RE AND TYPE CHECK")
    for file_name in file_names:
        check_format_re(file_name, [100, 200, 300])
