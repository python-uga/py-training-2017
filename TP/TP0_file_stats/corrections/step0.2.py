#!/usr/bin/python3
""" computes basic statistics on file
    that contains a set of lines, each line containing
    one float
"""


def compute_stats(file_name):
    """
    computes the statistics of data in file_name
    :param file_name: the name of the file to process
    :type file_name: str
    :return: the statistics
    :rtype: a tuple (number, sum, average)
    """
    total_sum = 0.0
    number = 0
    with open(file_name) as handle:
        for line in handle:
            elem = float(line)
            total_sum += elem
            number += 1

    return (number, total_sum, total_sum/float(number))


file_names = ['../data/file0.{}.txt'.format(x) for x in range(1, 4)]
# equivalent to
# file_names = ["../data/file0.1.txt", "../data/file0.2.txt",
#               "../data/file0.3.txt"]

numbers = []
sums = []

for file_name in file_names:
    (local_number, local_sum, local_avg) = compute_stats(file_name)
    numbers.append(local_number)
    sums.append(local_sum)

for (file_name, local_number, local_sum) in zip(file_names, numbers, sums):
    print('file = "{}"\nnb = {:<5}; sum = {:<7,.2f}; avg = {:<5,.2f}'.format(
        file_name, local_number, local_sum,
        local_number/local_sum))

all_sum = sum(sums)
all_numbers = sum(numbers)
print('# total over all files:\n'
      'nb = {: <5}; sum = {:<7,.2f}; avg = {:.2f}'.format(
          all_numbers, all_sum, all_sum/all_numbers))
