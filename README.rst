Python training UGA 2017
========================

**A training to acquire strong basis in Python to use it efficiently**

The code of this training has been written for Python 3.

Formated output of the notebooks is available here: https://python-uga.gricad-pages.univ-grenoble-alpes.fr/py-training-2017


To display and/or modify the presentations, use the Makefile. See::

  make help

Repository
----------

The repository hosted in the Gitlab of UGA:
https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga

- Clone the repository with Git with https::

    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-2017.git

- or clone the repository with Git with ssh::

    git clone git@gricad-gitlab.univ-grenoble-alpes.fr:python-uga/py-training-2017.git

- or clone the repository with Mercurial (and the extension hg-git, as explained
  `here <http://fluiddyn.readthedocs.io/en/latest/mercurial_bitbucket.html>`_),
  with the same commands but replacing `git` by `hg`::

    hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-2017.git

  or::

    hg clone git@gricad-gitlab.univ-grenoble-alpes.fr:python-uga/py-training-2017.git



To be done before the first day of the training session
-------------------------------------------------------

We will have to use Python 3 (with Miniconda3), a good Python IDE (either
Spyder or VSCode), Jupyter and a version-control tool (Git or Mercurial).

In the following, we give indications about how to install these tools and how
to get the repository of this training locally on your computer. Please, try to
do this before the training and tell us if you encounter problems.

Install conda
^^^^^^^^^^^^^

The first step is to install `miniconda3 <https://docs.anaconda.com/free/miniconda/>`_. Miniconda3 is a lightweight version of conda, a software that create contained environments to install python packages. We will use it to install the packages required by the training program, while ensuring that they do not conflict with packages already installed on your system.

Clone this repository
^^^^^^^^^^^^^^^^^^^^^

Once miniconda is installed and setup, you should be able to clone the
repository of the training with the miniconda prompt (see the clone instructions above).

Once the repository is cloned you should have a new directory called `py-training-2017`

Install the required packages in a new conda environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following instructions open the `py-training-2017` directory and create a new conda environment named `py-training-2017` inside which it install all the packages required by this training course.

.. code-block::

   cd py-training-2017
   conda env create -f environment.yaml

Finally, you can activate the Python environment we will use during this training with::

  conda activate py-training-2017

Build the presentations (facultative)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Now that everything is installed, you should be able to build the presentations::

  make presentations 

should build the presentations and::

  make serve

should start a server to visualize them in a browser.


Authors
-------

Pierre Augier (LEGI), Cyrille Bonamy (LEGI), Eric Maldonado (INRAE), Franck
Thollard (ISTERRE), Mathieu Istas (ISTERRE), Margaux Mouchène (ISTERRE), Loïc Huder (ESRF), Colin Thomas (ISTERRE)

Franck Thollard franck.thollard@univ-grenoble-alpes.fr
Colin Thomas colin.thomas1@univ-grenoble-alpes.fr